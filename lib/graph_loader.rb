require_relative '../process_logger'
require 'nokogiri'
require_relative 'graph'
require_relative 'visual_graph'
require_relative 'distance'

# Class to load graph from various formats. Actually implemented is Graphviz formats. Future is OSM format.
class GraphLoader
	attr_reader :highway_attributes

	# Create an instance, save +filename+ and preset highway attributes
	def initialize(filename, highway_attributes)
		@filename = filename
		@highway_attributes = highway_attributes
	end

	def get_tile_number(lat_deg, lng_deg, zoom)
		lat_rad = lat_deg/180 * Math::PI
		n = 2.0 ** zoom
		x = ((lng_deg + 180.0) / 360.0 * n).to_i
		y = ((1.0 - Math::log(Math::tan(lat_rad) + (1 / Math::cos(lat_rad))) / Math::PI) / 2.0 * n) * - 1 .to_i

		{:x => x, :y =>y}
	end

	# Load graph from Graphviz file which was previously constructed from this application, i.e. contains necessary data.
	# File needs to contain 
	# => 1) For node its 'id', 'pos' (containing its re-computed position on graphviz space) and 'comment' containig string with comma separated lat and lon
	# => 2) Edge (instead of source and target nodes) might contains info about 'speed' and 'one_way'
	# => 3) Generaly, graph contains parametr 'bb' containing array withhou bounds of map as minlon, minlat, maxlon, maxlat
	#
	# @return [+Graph+, +VisualGraph+]
	def load_graph_viz()
		ProcessLogger.log("Loading graph from GraphViz file #{@filename}.")
		gv = GraphViz.parse(@filename)

		# aux data structures
		hash_of_vertices = {}
		list_of_edges = []
		hash_of_visual_vertices = {}
		list_of_visual_edges = []

		# process vertices
		ProcessLogger.log("Processing vertices")
		gv.node_count.times { |node_index|
			node = gv.get_node_at_index(node_index)
			vid = node.id

			v = Vertex.new(vid) unless hash_of_vertices.has_key?(vid)
			ProcessLogger.log("\t Vertex #{vid} loaded")
			hash_of_vertices[vid] = v

			geo_pos = node["comment"].to_s.delete("\"").split(",")
			pos = node["pos"].to_s.delete("\"").split(",")
			hash_of_visual_vertices[vid] = VisualVertex.new(vid, v, geo_pos[0], geo_pos[1], pos[1], pos[0])
			ProcessLogger.log("\t Visual vertex #{vid} in ")
		}

		# process edges
		gv.edge_count.times { |edge_index|
			link = gv.get_edge_at_index(edge_index)
			vid_from = link.node_one.delete("\"")
			vid_to = link.node_two.delete("\"")
			speed = 50
			one_way = false
			link.each_attribute { |k,v|
				speed = v if k == "speed"
				one_way = true if k == "oneway"
			}
			e = Edge.new(vid_from, vid_to, speed, one_way, 0)
			list_of_edges << e
			list_of_visual_edges << VisualEdge.new(e, hash_of_visual_vertices[vid_from], hash_of_visual_vertices[vid_to])
		}

		# Create Graph instance
		g = Graph.new(hash_of_vertices, list_of_edges)

		# Create VisualGraph instance
		bounds = {}
		bounds[:minlon], bounds[:minlat], bounds[:maxlon], bounds[:maxlat] = gv["bb"].to_s.delete("\"").split(",")
		vg = VisualGraph.new(g, hash_of_visual_vertices, list_of_visual_edges, bounds)

		return g, vg
	end





	def load_graph(is_oriented)
		ProcessLogger.log("Loading graph from OSM file #{@filename}.")
		doc = Nokogiri::XML(File.open(@filename))

		highway_types = @highway_attributes.map { |ele| "@v='#{ele}'" }.join(" or ")

		highways =  doc.xpath("/osm/way/tag[@k='highway']/..")
		ways = highways.xpath("tag[#{highway_types}]/..")



		# aux data structures
		hash_of_vertices = {}
		list_of_edges = []
		hash_of_visual_vertices = {}
		list_of_visual_edges = []

		# process vertices
		ProcessLogger.log("Processing vertices")

		ways.each do |way|
			oneway = false
			if is_oriented
				oneway = true if way.at_xpath("tag[@k='oneway'][@v='yes']")
			end

			maxspeed = 50
			maxspeed_el =  way.at_xpath("tag[@k='maxspeed']/@v")
			if maxspeed_el
				maxspeed = maxspeed_el.content
			end

			nodes = way.xpath("nd").map { |node| node["ref"] }
			previous_vertex = nil
			previous_vertex_id = nil

			nodes.each do |node|
				vid = node
				unless hash_of_vertices.has_key?(node)
					v = Vertex.new(node)
					hash_of_vertices[node] = v
					osm_vertex = doc.at_xpath("/osm/node[@id='#{node}']")
					lat = osm_vertex["lat"]
					lon = osm_vertex["lon"]
					hash_of_visual_vertices[vid] = VisualVertex.new(vid, v, lat, lon, lat , lon)
				end
				if previous_vertex != nil
					lat1 = hash_of_visual_vertices[previous_vertex_id].lat.to_f
					lon1 = hash_of_visual_vertices[previous_vertex_id].lon.to_f
					lat2 = hash_of_visual_vertices[vid].lat.to_f
					lon2 = hash_of_visual_vertices[vid].lon.to_f
					length = haversine_distance(lat1, lon1, lat2, lon2)
					e = Edge.new(previous_vertex, hash_of_vertices[node], maxspeed, oneway, length)
					list_of_edges << e
					list_of_visual_edges << VisualEdge.new(e, hash_of_visual_vertices[previous_vertex_id], hash_of_visual_vertices[vid], oneway)
				end


				previous_vertex = hash_of_vertices[node]
				previous_vertex_id = vid
			end
		end


		# Create Graph instance
		g = Graph.new(hash_of_vertices, list_of_edges)

		bounds =  doc.at_xpath("/osm/bounds")


		vg = VisualGraph.new(g, hash_of_visual_vertices, list_of_visual_edges, bounds)

		return g, vg

	end


end
