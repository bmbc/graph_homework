# Class representing visual representation of a vertex.
class VisualVertex
  # ID of +self+ as well as +vertex+
  attr_reader :id
  # Corresponding vertex
  attr_reader :vertex
  # Lattitude of visual vertex
  attr_reader :lat
  # Longitute of visual vertex
  attr_reader :lon
  # X-axis position of +self+
  attr_reader :x
  # Y-axis position of +self+
  attr_reader :y
  # Emphasized
  attr_accessor :start_node
  attr_accessor :stop_node

  # create instance of +self+ by simple storing of all parameters
  def initialize(id, vertex, lat, lon, x, y, start_node=false, stop_node=false)
    @id = id
    @vertex = vertex
    @lat = lat
    @lon = lon
    @x = x
    @y = y
    @start_node = start_node
    @stop_node = stop_node
  end
end

