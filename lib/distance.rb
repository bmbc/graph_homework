
def haversine_distance(lat1, lon1, lat2, lon2, miles=false)
  # Calculate radial arcs for latitude and longitude
  dlat = (lat2 - lat1) * Math::PI / 180
  dlon = (lon2 - lon1) * Math::PI / 180


  a = Math.sin(dlat / 2) *
      Math.sin(dlat / 2) +
      Math.cos(lat1 * Math::PI / 180) *
          Math.cos(lat2 * Math::PI / 180) *
          Math.sin(dlon / 2) * Math.sin(dlon / 2)

  c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))

  d = 6371 * c * (miles ? 1 / 1.6 : 1)
end