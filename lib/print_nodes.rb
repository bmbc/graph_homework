require_relative 'visual_graph'

class NodePrinter
  def initialize(vg)
    @vg = vg
  end

  def print_nodes
    @vg.visual_vertices.each do |key, value|
      puts "#{value.id}: #{value.lat}, #{value.lon}"
    end
  end


end