require_relative 'graph'
require_relative 'visual_graph'

class DepthFirstSearch
  def initialize(graph, vg)
    @graph = graph
    @visited = {}
    @components = {}
    @vg = vg
    @c = -1
  end

  def dfs
    @graph.vertices.each do |k, v|
      @visited[v] = false
    end
    @graph.vertices.each do |k, v|
      unless @visited[v]
        @c += 1
        @components[@c] = []
        visit(v)
      end
    end


    hash_of_vertices = {}
    list_of_edges = []
    @components.max_by { |key, value| value.length }[1].each do | ver |
      hash_of_vertices[ver.id] = ver
    end
    p @graph.vertices.length
    p hash_of_vertices.length
    hash_of_visual_vertices = @vg.visual_vertices.select { |key, value| hash_of_vertices.has_key?(key)}
    p hash_of_visual_vertices.length
    puts
    p @graph.edges.select { |edge| hash_of_vertices.select { |key, value| edge.v1 == value.id } }.length

    @graph.edges.each do |edge|
      if hash_of_vertices.has_key?(edge.v1.id)
        list_of_edges << edge
      end
       unless edge.one_way
        if hash_of_vertices.has_key?(edge.v2.id)
          list_of_edges << edge
        end
       end
    end

    p @graph.edges.length
    p list_of_edges.length
    list_of_visual_edges = @vg.visual_edges.select { |vis_edge| list_of_edges.include?(vis_edge.edge) }
    p list_of_visual_edges.length

    g = Graph.new(hash_of_vertices, list_of_edges)
    bounds = @vg.bounds
    vis_g = VisualGraph.new(g, hash_of_visual_vertices, list_of_visual_edges, bounds)

    return g, vis_g

  end

  def visit(v)
    @visited[v] = true
    @components[@c] << v
    neighbours = []
    #adding one-way neighbours
    oneway_neighbours =  @graph.edges.select { |edge| edge.v1 == v }.map { |edge| edge.v2 }
    oneway_neighbours.each do |ow_neighbour|
      neighbours << ow_neighbour
    end
    #adding two-way neighbours
    twoway_edges = @graph.edges.select { |edge| edge.one_way == false }
    twoway_neighbours =  twoway_edges.select { |edge| edge.v2 == v }.map { |edge| edge.v1 }
    twoway_neighbours.each do |tw_neighbour|
      neighbours << tw_neighbour
    end
    neighbours.each do |neighbour|
      unless @visited[neighbour]
        visit(neighbour)
      end
    end
  end





end



