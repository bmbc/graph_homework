require 'ruby-graphviz'
require_relative 'visual_edge'
require_relative 'visual_vertex'

# Visual graph storing representation of graph for plotting.
class VisualGraph
  # Instances of +VisualVertex+ classes
  attr_reader :visual_vertices
  # Instances of +VisualEdge+ classes
  attr_reader :visual_edges
  # Corresponding +Graph+ Class
  attr_reader :graph
  # Scale for printing to output needed for GraphViz
  attr_reader :scale
  # Bounds
  attr_reader :bounds

  # Create instance of +self+ by simple storing of all given parameters.
  def initialize(graph, visual_vertices, visual_edges, bounds)
  	@graph = graph
    @visual_vertices = visual_vertices
    @visual_edges = visual_edges
    @bounds = bounds
    @scale = ([bounds[:maxlon].to_f - bounds[:minlon].to_f, bounds[:maxlat].to_f - bounds[:minlat].to_f].min).abs / 10.0
  end

  # Export +self+ into Graphviz file given by +export_filename+.
  def export_graphviz(export_filename)
    # create GraphViz object from ruby-graphviz package
    graph_viz_output = GraphViz.new( :G, 
    								                  use: :neato, 
		                                  truecolor: true,
                              		    inputscale: @scale,
                              		    margin: 0,
                              		    bb: "#{@bounds[:minlon]},#{@bounds[:minlat]},
                                  		    #{@bounds[:maxlon]},#{@bounds[:maxlat]}",
                              		    outputorder: :nodesfirst)

    # append all vertices
    @visual_vertices.each { |k,v|
      if v.start_node
        graph_viz_output.add_nodes( v.id , :shape => 'egg',
                                    :color => 'green2',
                                    :comment => "#{v.lat},#{v.lon}!",
                                    :pos => "#{v.y},#{v.x}!")
      elsif v.stop_node
        graph_viz_output.add_nodes( v.id , :shape => 'octagon',
                                    :color => 'red3',
                                    :comment => "#{v.lat},#{v.lon}!",
                                    :pos => "#{v.y},#{v.x}!")
      else
              graph_viz_output.add_nodes( v.id , :shape => 'point',
                                          :comment => "#{v.lat},#{v.lon}!",
                                          :pos => "#{v.y},#{v.x}!")

      end

	  }

    # append all edges
	  @visual_edges.each { |edge|
      if edge.directed
        graph_viz_output.add_edges( edge.v1.id, edge.v2.id, 'arrowhead' => 'normal' )
      else
        graph_viz_output.add_edges( edge.v1.id, edge.v2.id, 'arrowhead' => 'none' )
      end
	  }

    # export to a given format
    format_sym = export_filename.slice(export_filename.rindex('.')+1,export_filename.size).to_sym
    graph_viz_output.output( format_sym => export_filename )
  end
end
