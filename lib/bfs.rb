require_relative 'graph'
require_relative 'visual_graph'

class BreadthFirstSearch
  def initialize(graph, vg)
    @graph = graph
    @visited = {}
    @components = {}
    @edge_to_pred = nil
    @edges = {}
    @vg = vg
    @c = -1
    @queue = Queue.new
    @predecesor = {}
  end

  def bfs
    @graph.vertices.each { |k, v| @visited[v] = false }
    @graph.vertices.each do |k, v|
      unless @visited[v]
        @c += 1
        @edges[@c] = []
        @components[@c] = []
        @queue << v
        until @queue.length == 0
          one_v = @queue.pop
          unless @visited[one_v]
            @visited[one_v] = true
            @components[@c] << one_v
            # if @predecesor[one_v] != nil
            #   # @edges[@c] << @graph.edges.select { |edge| edge.v2 == one_v && edge.v2 == one_v }
            # end

            neighbours = []
            #adding one-way neighbours
            oneway_neighbours =  @graph.edges.select { |edge| edge.v1 == one_v }.map { |edge| edge.v2 }
            oneway_neighbours.each do |ow_neighbour|
              neighbours << ow_neighbour
            end
            #adding two-way neighbours
            twoway_edges = @graph.edges.select { |edge| edge.one_way == false }
            twoway_neighbours =  twoway_edges.select { |edge| edge.v2 == one_v }.map { |edge| edge.v1 }
            twoway_neighbours.each do |tw_neighbour|
              neighbours << tw_neighbour
            end
            neighbours.each do |u|
              unless @visited[u]
                @predecesor[u] = one_v
                @queue << u
              end
            end
          end
        end
      end
    end

    hash_of_vertices = {}
    @components.max_by { |key, value| value.length }[1].each { | ver | hash_of_vertices[ver.id] = ver }
    hash_of_visual_vertices = @vg.visual_vertices.select { |key, value| hash_of_vertices.has_key?(key)}
    list_of_edges = []


    @graph.edges.each do |edge|
      if hash_of_vertices.has_key?(edge.v1.id) && hash_of_vertices.has_key?(edge.v2.id)
        list_of_edges << edge
      end
    end
    list_of_visual_edges = @vg.visual_edges.select { |vis_edge| list_of_edges.include?(vis_edge.edge) }


    g = Graph.new(hash_of_vertices, list_of_edges)
    bounds = @vg.bounds
    vis_g = VisualGraph.new(g, hash_of_visual_vertices, list_of_visual_edges, bounds)

    return g, vis_g



  end

end




