#just for testing some stuff - not gonna be included in the project

def haversine_distance(lat1, lon1, lat2, lon2, miles=false)
  # Get latitude and longitude
  # lat1, lon1 = geo_a
  # lat2, lon2 = geo_b

  # Calculate radial arcs for latitude and longitude
  dlat = (lat2 - lat1) * Math::PI / 180
  dlon = (lon2 - lon1) * Math::PI / 180


  a = Math.sin(dlat / 2) *
      Math.sin(dlat / 2) +
      Math.cos(lat1 * Math::PI / 180) *
          Math.cos(lat2 * Math::PI / 180) *
          Math.sin(dlon / 2) * Math.sin(dlon / 2)

  c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a))

  d = 6371 * c * (miles ? 1 / 1.6 : 1)
end


p haversine_distance(50.0940075, 14.4370458, 50.0932451, 14.4270164)

# <node id="247772" lat="50.0940075" lon="14.4370458" version="3" timestamp="2012-01-04T11:29:10Z" changeset="10289839" uid="17615" user="Petr Dlouhý"/>
# <node id="247776" lat="50.0932451" lon="14.4270164" version="6" timestamp="2017-04-11T02:30:17Z" changeset="47643595" uid="10353" user="gorn"/>