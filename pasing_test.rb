#just for testing some stuff - not gonna be included in the project


require 'nokogiri'


# Class representing visual representation of edge
class VisualEdge
  # Starting +VisualVertex+ of this visual edge
  attr_reader :v1
  # Target +VisualVertex+ of this visual edge
  attr_reader :v2
  # Corresponding edge in the graph
  attr_reader :edge
  # Boolean value given directness
  attr_reader :directed
  # Boolean value emphasize character - drawn differently on output (TODO)
  attr_reader :emphesized

  # create instance of +self+ by simple storing of all parameters
  def initialize(edge, v1, v2)
    @edge = edge
    @v1 = v1
    @v2 = v2
  end
end




# Class representing edge of a graph
class Edge
  # Staring vertex of an edge
  attr_reader :v1
  # Target vertex of an edge
  attr_reader :v2
  # Maximal speed on this edge
  attr_reader :max_speed
  # Indicator of on direction edge - i.e. can be passed only from +v1+ to +v2+
  attr_reader :one_way

  # create instance of +self+ by simple storing of all parameters
  def initialize(v1, v2, max_speed, one_way)
    @v1 = v1
    @v2 = v2
    @max_speed = max_speed
    @one_way = one_way
  end
end




# Class representing visual representation of a vertex.
class VisualVertex
  # ID of +self+ as well as +vertex+
  attr_reader :id
  # Corresponding vertex
  attr_reader :vertex
  # Lattitude of visual vertex
  attr_reader :lat
  # Longitute of visual vertex
  attr_reader :lon
  # X-axis position of +self+
  attr_reader :x
  # Y-axis position of +self+
  attr_reader :y

  # create instance of +self+ by simple storing of all parameters
  def initialize(id, vertex, lat, lon, x, y)
    @id = id
    @vertex = vertex
    @lat = lat
    @lon = lon
    @x = x
    @y = y
  end
end

# Class representing vertex in a graph
class Vertex
  # id of the +Vertex+
  attr_reader :id

  # create instance of +self+ by simple storing of all parameters
  def initialize id
    @id = id
  end
end

def get_tile_number(lat_deg, lng_deg, zoom)
  lat_rad = lat_deg/180 * Math::PI
  n = 2.0 ** zoom
  x = ((lng_deg + 180.0) / 360.0 * n).to_i
  y = ((1.0 - Math::log(Math::tan(lat_rad) + (1 / Math::cos(lat_rad))) / Math::PI) / 2.0 * n) * - 1 .to_i

  {:x => x, :y =>y}
end




nodes = []

doc = Nokogiri::XML(File.open("data/near_ucl.osm"))

# doc.root.xpath("way"). each do |way|
#   tag = way.at_xpath("tag")
#   puts "#{tag["k"]}    -  #{tag["v"]}  "
# end

test = "residential"
@highway_attributes = ['residential', 'motorway', 'trunk', 'primary', 'secondary', 'tertiary', 'unclassified'].map { |ele| "@v='#{ele}'" }.join(" or ")


# TODO
puts "non-oriented graph"

@filename = "data/near_ucl.osm"

doc = Nokogiri::XML(File.open(@filename))

#//Element[@attribute1="abc" and @attribute2="xyz" and text()="Data"]
ways2 =  doc.xpath("/osm/way")

ways008 =  doc.xpath("/osm/way/tag[@k='highway']/..")

#ways0 = ways.xpath("tag[@v='residential' or @v='motorway' or @v='trunk' or @v='primary' or @v='secondary' or @v='tertiary' or @v='unclassified']/..")
ways = ways008.xpath("tag[#{@highway_attributes}]/..")
nodes = ways

#
# This works
ways00 = ways.xpath("tag[@v='residential' or @v='motorway' or @v='trunk' or @v='primary' or @v='secondary' or @v='tertiary' or @v='unclassified']")


ways4 =  doc.xpath("/osm/way/tag[@k='highway' and @v='residential']/..")
ways5 =  doc.xpath("/osm/way/tag[@k='highway' and @v='motorway']/..")
ways6 =  doc.xpath("/osm/way/tag[@k='highway' and @v='trunk']/..")
ways7 =  doc.xpath("/osm/way/tag[@k='highway' and @v='primary']/..")
ways8 =  doc.xpath("/osm/way/tag[@k='highway' and @v='secondary']/..")
ways9 =  doc.xpath("/osm/way/tag[@k='highway' and @v='tetriary']/..")
ways10 =  doc.xpath("/osm/way/tag[@k='highway' and @v='unclassified']/..")





bounds =  doc.at_xpath("/osm/bounds")
bounds.map {|key, value| puts value.to_f }
p bounds[:minlat]



# aux data structures
hash_of_vertices = {}
list_of_edges = []
hash_of_visual_vertices = {}
list_of_visual_edges = []

# # process vertices
# puts "all ways"
# p ways2.length
# puts "all highways"
# p ways.length
# puts
# puts "contains test"
# p ways4.length
# p ways5.length
# p ways6.length
# p ways7.length
# p ways8.length
# p ways9.length
# p ways10.length
# puts
#
# p ways00.length
# p @highway_attributes
# p ways007.length


ways.each do |way|
  oneway = false
  oneway = true if way.at_xpath("tag[@k='oneway'][@v='yes']")
  maxspeed = 50
  maxspeed_el =  way.at_xpath("tag[@k='maxspeed']/@v")
  if maxspeed_el
    maxspeed = maxspeed_el.content
  end

  nodes = way.xpath("nd").map { |node| node["ref"] }
  previous_vertex = nil
  previous_vertex_id = nil

  nodes.each do |node|
    vid = node
    unless hash_of_vertices.has_key?(node)

      v = Vertex.new(node)
      hash_of_vertices[node] = v

      osm_vertex = doc.at_xpath("/osm/node[@id='#{node}']")
      lat = osm_vertex["lat"]
      lon = osm_vertex["lon"]

      pos = get_tile_number(lat.to_f, lon.to_f, 25)

      hash_of_visual_vertices[vid] = VisualVertex.new(vid, v, lat, lon, pos[:y] , pos[:x])
    end

    if previous_vertex != nil
      e = Edge.new(previous_vertex, v, maxspeed, oneway)
      list_of_edges << e
      #list_of_visual_edges << VisualEdge.new(e, hash_of_visual_vertices[vid_from], hash_of_visual_vertices[vid_to])
      list_of_visual_edges << VisualEdge.new(e, hash_of_visual_vertices[previous_vertex_id], hash_of_visual_vertices[vid])
    end

    previous_vertex = v
    previous_vertex_id = vid
  end

end



# Create Graph instance
# g = Graph.new(hash_of_vertices, list_of_edges)

#select max lat, lon
max_lat = hash_of_visual_vertices.max_by { |k, v| v.lat }[1].lat.to_f
min_lat = hash_of_visual_vertices.min_by { |k, v| v.lat }[1].lat.to_f



max_lon = hash_of_visual_vertices.max_by { |k, v| v.lon }[1].lon.to_f
min_lon = hash_of_visual_vertices.min_by { |k, v| v.lon }[1].lon.to_f

max_things = get_tile_number(max_lat, max_lon, 25)
min_things = get_tile_number(min_lat, min_lon, 25)

# # Create VisualGraph instance
# bounds = {}
# bounds[:minlon] = min_things[:y]
# bounds[:minlat] = min_things[:x]
# bounds[:maxlon] = max_things[:y]
# bounds[:maxlat] = max_things[:x]
#
# vg = VisualGraph.new(g, hash_of_visual_vertices, list_of_visual_edges, bounds)
#
# return g, vg





# #ways3 =  doc.xpath("//way[//tag[@k='highway' and @v='residential']]")
# ways =  doc.xpath("/osm/way/tag[@k='highway'][@v='#{test}']/..")
#
# all_nodes = doc.xpath("/osm/node")
# ways4 =  doc.xpath("/osm/way/tag[@k='highway'][contains('#{highway_attributes}', v)]/..")
# #ways2 =  doc.xpath("//way/tag[@k='highway' and @v='residential']")
#
# #highway_ways =  doc.xpath("//way").select { |node| node.at_xpath("tag") != nil && node.at_xpath("tag")["k"] == "highway" && node.at_xpath("tag")["v"] == "residential" }
#
# #p highway_attributes
#
# #top = highway_ways.select { |node| node.at_xpath("tag")["k"] == "highway" && node.at_xpath("tag")["k"] == "residential" }
# #puts ways4.length
#
#
#
# def get_tile_number(lat_deg, lng_deg, zoom)
#   lat_rad = lat_deg/180 * Math::PI
#   n = 2.0 ** zoom
#   x = ((lng_deg + 180.0) / 360.0 * n).to_i
#   y = ((1.0 - Math::log(Math::tan(lat_rad) + (1 / Math::cos(lat_rad))) / Math::PI) / 2.0 * n).to_i
#
#   {:x => x, :y =>y}
# end
#
# lata="54.0865517".to_f
# longa="14.4625145".to_f
#
# # nodes =
# # nodes1 = ways4.first.select {|node| }
#
# hash_of_vertices = {}
# list_of_edges = []
# hash_of_visual_vertices = {}
# list_of_visual_edges = []
#
#
# ways4[10].xpath("nd"). each do |ele|
#   p ele["ref"]
# end
#
# # Class representing vertex in a graph
# class Vertex
#   # id of the +Vertex+
#   attr_reader :id
#
#   # create instance of +self+ by simple storing of all parameters
#   def initialize id
#     @id = id
#   end
# end
#
#
#
# def load_graph()
#   # TODO
#   puts "non-oriented graph"
#   ProcessLogger.log("Loading graph from OSM file #{@filename}.")
#   doc = Nokogiri::XML(File.open(@filename))
#
#   ways =  doc.xpath("/osm/way/tag[@k='highway'][contains('#{@highway_attributes.join(" ")}', v)]/..")
#
#
#   # aux data structures
#   hash_of_vertices = {}
#   list_of_edges = []
#   hash_of_visual_vertices = {}
#   list_of_visual_edges = []
#
#   # process vertices
#   ProcessLogger.log("Processing vertices")
#
#   ways.each do |way|
#     oneway = false
#     oneway = true if way.at_xpath("tag[@k='oneway'][@v='yes']")
#     maxspeed = 50
#     maxspeed_el =  way.at_xpath("tag[@k='maxspeed']/@v")
#     if maxspeed_el
#       maxspeed = maxspeed_el.content
#     end
#
#     nodes = way.xpath("nd").map { |node| node["ref"].to_i }
#     previous_vertex = nil
#
#     nodes.each do |node|
#       unless hash_of_vertices.has_key?(node)
#         vid = node.to_i
#         v = Vertex.new(node)
#         hash_of_vertices[node] = v
#
#         osm_vertex = doc.at_xpath("/osm/node[@id='#{node}']")
#         lat = osm_vertex["lat"].to_f
#         lon = osm_vertex["lon"].to_f
#
#         pos = get_tile_number(lat.to_f, lon.to_f, 25)
#
#         hash_of_visual_vertices[vid] = VisualVertex.new(vid, v, lat, lon, pos[:y] , pos[:x])
#       end
#
#       if previous_vertex != nil
#         e = Edge.new(previous_vertex, v, maxspeed, oneway)
#         list_of_edges << e
#         list_of_visual_edges << VisualEdge.new(e, hash_of_visual_vertices[previous_vertex], hash_of_visual_vertices[v])
#       end
#
#       previous_vertex = v
#     end
#
#   end
#
#   # Create Graph instance
#   g = Graph.new(hash_of_vertices, list_of_edges)
#
#   #select max lat, lon
#   max_lat = hash_of_visual_vertices.max_by { |k, v| v.lat }[1].lat.to_s
#   min_lat = hash_of_visual_vertices.min_by { |k, v| v.lat }[1].lat.to_s
#
#   max_lon = hash_of_visual_vertices.max_by { |k, v| v.lon }[1].lon.to_s
#   min_lon = hash_of_visual_vertices.min_by { |k, v| v.lon }[1].lon.to_s
#
#   # Create VisualGraph instance
# #   bounds = {}
# #   bounds[:minlon], bounds[:minlat], bounds[:maxlon], bounds[:maxlat] = min_lon, min_lat, max_lon, max_lat
# #   vg = VisualGraph.new(g, hash_of_visual_vertices, list_of_visual_edges, bounds)
# #
# #   return g, vg
# #
# # end
#
# ways4.each do |way|
#   oneway = false
#   if way.at_xpath("tag[@k='oneway'][@v='yes']")
#     oneway = true
#   end
#   #maxspeed
#   maxspeed = 50
#   maxspeed_el =  way.at_xpath("tag[@k='maxspeed']/@v")
#   if maxspeed_el
#     maxspeed = maxspeed_el.content
#   end
#
#
#   nodes = way.xpath("nd").map { |node| node["ref"].to_i }
#
#   previous_node = nil
#
#   nodes.each do |node|
#
#     unless hash_of_vertices.has_key?(node)
#       vid = node.to_i
#       v = Vertex.new(node)
#       hash_of_vertices[node] = v
#
#
#       osm_vertex = doc.at_xpath("/osm/node[@id='#{node}']")
#       lat = osm_vertex["lat"].to_f
#       lon = osm_vertex["lon"].to_f
#
#       pos = get_tile_number(lat.to_f, lon.to_f, 25)
#
#       hash_of_visual_vertices[vid] = VisualVertex.new(vid, v, lat, lon, pos[:y] , pos[:x])
#     end
#      # v = Vertex.new(node) unless hash_of_vertices.has_key?(node)
#      # hash_of_vertices[node] = v
#      #
#      #
#      # osm_vertex = doc.at_xpath("/osm/node[@id='#{node}']")
#      # lat = osm_vertex["lat"].to_f
#      # lon = osm_vertex["lon"].to_f
#      #
#      # pos = get_tile_number(lat.to_f, lon.to_f, 25)
#      #
#      # hash_of_visual_vertices[vid] = VisualVertex.new(vid, v, lat, lon, pos[:y] , pos[:x])
#
#     if previous_node != nil
#       e = Edge.new(previous_node, node, maxspeed, oneway)
#       list_of_edges << e
#       list_of_visual_edges << VisualEdge.new(e, hash_of_visual_vertices[previous_node], hash_of_visual_vertices[node])
#     end
#
#
#
#     previous_node = node
#   end
#
# end
#
#  #p hash_of_vertices
# hash_of_visual_vertices.each do |key, value|
#   p value.lat
# end
#
# #select max lat
# puts
# p max_lat = hash_of_visual_vertices.max_by { |k, v| v.lat }[1].lat
# p min_lat = hash_of_visual_vertices.min_by { |k, v| v.lat }[1].lat
#
# p max_lon = hash_of_visual_vertices.max_by { |k, v| v.lon }[1].lon
# p min_lon = hash_of_visual_vertices.min_by { |k, v| v.lon }[1].lon
#
# puts hash_of_visual_vertices.first
#
# # p ways4[10]


